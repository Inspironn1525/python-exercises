# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 11:26:56 2019

@author: sep16
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
url = "Poverty_no_missing.csv"

poverty_data = pd.read_csv(url, sep=",")
#print(poverty_data.columns)
#poverty_data.loc[:, 'LExpF'].plot(kind='bar')


# Ploting Female life expentancy against GNP by Matplotlib
#plt.plot(poverty_data.loc[:, 'LExpF'], 
#         poverty_data.loc[:, 'GNP'],
#         'o')

# Ploting average life expentancy for each country against GNP
avr_LExp = (poverty_data.loc[:, 'LExpF'] + poverty_data.loc[:, 'LExpM'])/2
#print(avr_LExp)
plt.plot(avr_LExp, 
         poverty_data.loc[:, 'GNP'],
         'ro')