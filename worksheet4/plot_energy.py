import matplotlib.pyplot as plt
from matplotlib import cm # colour map
import csv

def make_colours(num_items):
    # Generates colours for num_item items
    # We'll generate enough numbers between 0 and 1 and choose their colours from colormap Set1.
    colornums = []
    for n in range(num_items):
        colornums.append(n / num_items)
    cs = cm.Set1(colornums)
    return cs


def process_info(names, data):
    # What does this for-loop do?
    descriptions = []
    for i in range(len(data)):
        descriptions.append(names[i] + " (" + str(data[i]) + "%)")

    # We need as many colours as data items
    num_items = len(data)
    cs = make_colours(num_items)

    return data, descriptions, cs


def read_info(filename):
    kinds_of_energy = []
    percentages_of_energy = []
    
    # Here read in the file and collect the data

    with open(filename) as f:   # open the file
        csvr = csv.reader(f)        # initialise csv reading
        header = next(f)            # skip header line
        for row in csvr:            # process each row
            kinds_of_energy.append(row[0])  # energy type is 1st string in row
            percentages_of_energy.append(float(row[1])) # percentage is 2nd

    # TODO: optionally sort the data by percentage of energy
    #...
    zipped = zip(percentages_of_energy, kinds_of_energy)
    sort = sorted(zipped)
    #print(sort)
    percentages_of_energy = [x[0] for x in sort]
    kinds_of_energy = [x[1] for x in sort]
    return kinds_of_energy, percentages_of_energy



def make_pie_plot(values, labels, cols):
    # Draw the pie chart
    plt.pie(values, colors=cols)

    # Set aspect ratio to be equal so that pie is drawn as a circle.
    plt.axis('equal')
    
    # Provide a legend whose 'centre left' is shifted outside the axes 
    plt.legend(labels, loc='center left', bbox_to_anchor=(1,0.5))

    # Add a title
    plt.title('Proportions of electricity sources')

    # Save the image and make sure the extent of the image includes the legend,
    # which was shifted outside.
    plt.savefig("pie.png", bbox_inches='tight')
    

def make_barh_plot(values, labels, cols):
    plt.barh(labels, values, color = cols)
    
    #plt.legend(labels, loc='center left', bbox_to_anchor=(1,0.5))
    plt.title('Proportions of electricity sources')
    plt.savefig("barh.png", bbox_inches='tight')


def main():
    kinds, percents = read_info("energy.csv")
    data, names, colours = process_info(kinds, percents)

    make_pie_plot(data, names, colours)
    #make_barh_plot(data, names, colours)

if __name__ == "__main__":
    main()
