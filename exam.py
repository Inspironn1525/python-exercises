# QUESTION 1
# Implement your function here
def fraction(numerator, denominator):
    '''(int, int) -> str.
    Function returns a string representation of the fraction.
    '''
    numerator = str(numerator)
    denominator = str(denominator)
    maxVal = max(len(numerator), len(denominator))
    numerator = numerator.center(maxVal)
    denominator = denominator.center(maxVal)
    text = numerator + "\n" + "-"*maxVal + "\n" + denominator
    return text



# Do not modify the test function
def test_fraction():
    num = int(input("Type in the numerator: "))
    den = int(input("Type in the denominator: "))

    print("The fraction is:")
    print(fraction(num, den))



# QUESTION 2
# Implement your function here
def word_count(word, list_of_words):
    '''(str, list) -> int
    Function returns the number of occurences of the provided string in provided list.
    '''
    word = word.upper()
    list_of_words = [list_of_words[i].upper() for i in range(len(list_of_words))]
    total = 0
    for i in range(len(list_of_words)):
        if list_of_words[i] in word:
            total += 1
    return total



# Do not modify the test function
def test_word_count():
    list_of_words = ["Hi", "Computational", "Hi", "no", "No", "NO"]
    word = input("What word do you want to count? ")
    total = word_count(word, list_of_words)
    if total == 0:
        print("'" + word + "'", "doesn't exist in the list.")
    else:
        print("'" + word + "'", "exists", total, "time(s) in the list.")


# QUESTION 3
# Implement your function here
def my_max(list_of_nums):
    '''(list) -> number
    Function return the greatest of the numbers in the list.
    '''
    maxVal = list_of_nums[0]
    for i in list_of_nums:
        if i > maxVal:
            maxVal = i
    return maxVal
        

# Do not modify the test function
def test_my_max():
    list_of_nums = [-999, 5, 2, 65, 2.5, 6, 7, 12223, 123, 5, -7, 8]
    print("The maximum value in the list is", my_max(list_of_nums))



# QUESTION 4
# Implement your function here
def calculator():
    state = True
    while state:
        print('====MENU====')
        print('1 - Add')
        print('2 - Substract')
        print('3 - Multiply')
        print('4 - Divide')
        print('Type any other character to quit')
        print('============')
        option = input('Choose an option: ')
        if option == "1":
            a = float(input("Type in a number: "))
            b = float(input("Type in another number: "))
            c = a + b
            print("Result:",c)
        elif option == "2":
            a = float(input("Type in a number: "))
            b = float(input("Type in another number: "))
            c = a - b
            print("Result:",c)
        elif option == "3":
            a = float(input("Type in a number: "))
            b = float(input("Type in another number: "))
            c = a * b
            print("Result:",c)
        elif option == "4":
            a = float(input("Type in a number: "))
            b = float(input("Type in another number: "))
            c = a / b
            print("Result:",c)
        else:
            state = False
            print("Bye...")
    
