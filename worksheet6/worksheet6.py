# -*- coding: utf-8 -*-
"""
Created on Tue Feb  5 11:19:27 2019

@author: sep16
"""

import pandas as pd
import numpy as np
pd.options.display.max_columns = 10
url = "GOOGL_2018.csv"
data_googl = pd.read_csv(url, sep = ",")
#print (data_googl)
#data_googl["volume"].plot()

data_frame = data_googl.loc[:, 'open':'close']
#print(data_frame)
#data_frame.plot()
data_frame['change'] = data_frame['open'] - data_frame['close']
#print(data_frame)
#data_frame.plot()
##### TASK 2 #####

file = "brain.txt"
data_brain = pd.read_csv(file, sep = '\t')
#print(data_brain.describe())
#print(data_brain[data_brain["Gender"] == 'Male'])
#print(data_brain[data_brain["Gender"] == 'Female'])
by_weight = data_brain.groupby("Weight")
#by_weight.boxplot(column = ['FSIQ', 'VIQ', 'PIQ'])

t = np.linspace(-6, 6, 10)
sin_t = np.sin(t)
cos_t = np.cos(t)
data = pd.DataFrame({'t': t, 'sin': sin_t, 'cos': cos_t})
print(data)#the data on itself presented in a table
print(data.shape) # the shape of a table (rows, columns)
print(data.columns) # the columns attribute gives the columns names (labels)
#print(data.loc[ :,'t':'sin'])
print(data.loc[0:2, ['t', 'sin']])