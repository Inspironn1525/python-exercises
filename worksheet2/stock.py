import numpy as np

def input_a_number():
   #english = input("Give me the name of the new item in English: ")
   #welsh = input("Give me the name of the new item in Welsh: ")
    new_price = int(input("Input the new price: "))
    amount = int(input("Amount of stock: "))
    print(new_price * amount)

stock = {
    "apple": 10,
    "strawberry": 35,
    "orange": 9,
    "beetroot": 20,
    "carrot": 13
}

price = {
    "apple": 1.4,
    "strawberry": 2.0,
    "orange": 1.0,
    "beetroot": 2.5,
    "carrot": 1.2
}

cymraeg = {
    "apple": "afal",
    "strawberry": "mefus",
    "orange": "oren",
    "beetroot": "betys",
    "carrot": "moron"
}

customer1 = ["apple", "orange", "apple", "beetroot", "beetroot"]
#(a) task
value = 0;
for k in stock:
    value += stock[k] * price[k]
    
print("This is the total value of stock:",value)

#(b) task

bill= 0;

for item in customer1:
   print(item, price[item])
   bill += price[item]

print("The total value is =", bill)   


for item in customer1:
    stock[item] -=1
#(c) task
def new_item():
    english = input("Give me the name of the new item in English: ")
    welsh = input("Give me the name of the new item in Welsh: ")
    new_price = int(input("Input the new price: "))
    amount = int(input("Amount of stock: "))
    stock[english] = amount
    price[english] = new_price
    cymraeg[english] = welsh
    
def print_menu():
    print("THE MENU")
    for key, value in price.items():
        print("The price of the "+ key + " is", value)
def print_welsh_menu():
    for i in cymraeg:
        print("The price of the ", cymraeg[i]," is", price[i])
        
#2b section
tree_heights = np.random.uniform(0.5, 5, 100)
sorted_tree = sorted(tree_heights)
print("The mean tree height is ", sum(sorted_tree)/len(sorted_tree))

forest_means = []
for i in range(1000):
    sorted_one = sorted(np.random.uniform(0.5, 5, 100))
    forest_means.append(sum(sorted_one)/len(sorted_one))
s_means = sorted(forest_means)
#rint(s_means)

#length = len(s_means) * 5 / 100
if(s_means[49]>2.5):
    print("No")
else:
    print("Yes")