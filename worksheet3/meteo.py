"""
Provide functions to read and analyse temperature and rainfall in England and Wales from 1845 to 2011

This module provides functions to read and analyse climate data in order to manage the data,
find the index of row by a given year and calculate mean tempperatures.

    * read_data    Read climate data from file
    * find_offset  Find index of row by given year
    * get_years    Returns the slice of the whole raintemp_1845_2011.csv file
    * mean_temps   Calculates mean temperatures for the winter and summer temperatures
    * compare_two_eras Calculates and prints summer and winter means in a touple for victorian and modern eras
"""

import numpy as np
import pydoc

# This function has an example docstring
def read_data(filename):
    """Read whitespace-separated data from a file, returning a numpy array.
    
    Args:
        filename (str): Name of the data file to be read
    
    Returns:
        numpy 2D array: Numeric data read from the file
    """
    a = np.loadtxt(filename, dtype=float, skiprows=1)
    return a
a = read_data("raintemp_1845_2011.csv")
#print(a)
def find_offset(year):
    """Find index in the array 
    Args:
        year (int): A year between the 1845 and 2011
        
    Returns:
        index(int): Index of the specific row from the 2D array
    """
    #a = read_data("raintemp_1845_2011.csv")
    #ind = np.where(a == str(year))
   #for i in range(0, 167):
    #    if(a[i][0] == year):
    return int(year) - 1845

def get_years(an_array, start_year, stop):
    """Function slices 2D numpy array
    Args:
        numpy 2D array: Numeric data read from the file
        start_year (int): A one year between the 1845 and 2011
        stop (int): Another year between the 1845 and 2011
        
    Returns:
        numpy 2D array: slice of the inputed array started from start_year to the stop (exclusive)
    """
    start_point = find_offset(start_year)
    end_point = find_offset(stop)
    return an_array[start_point:end_point]
b = get_years(a, 1845, 1851)
def mean_temps(an_array):
    """Function calculates summer mean and winter mean temperatures for the whole array
    Args:
        numpy 2D array: Numeric data read from the file
        
    Returns:
        touple of two values(int,int): mean for a winter and mean for a summer
    """
    mean_winter = sum(an_array[:,1])/len(an_array[:,1])
    mean_summer = sum(an_array[:,3])/len(an_array[:,3])
    return (mean_winter, mean_summer)
#c = a[0:,1]
print(mean_temps(a))
#print(mean_temps(get_years(a, 1847, 1850)))

def compare_two_eras():
    """Function prints the means for the victorian era and the modern era 
       
    """
    victorian_era = mean_temps(get_years(a, 1845, 1901))
    modern_era = mean_temps(get_years(a, 1870, 2012))
    print("Moder era", modern_era)
    print("Victorian era", victorian_era)
    print("Difference between summer means is ", abs(victorian_era[1] - modern_era[1]))
    print("Difference between winter means is ", abs(victorian_era[0] - modern_era[0]))