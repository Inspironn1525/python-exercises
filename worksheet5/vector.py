import math

class Vector:
    """A 2-dimensional vector with components x and y"""
    
    def __init__(self, x= 0.0, y= 0.0):
        """Create a new Vector with components of 0."""
        self.x = x
        self.y = y
    
    def __str__(self):
        return "Vector(%g,%g)"% (self.x,self.y)
    
    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y) 
        
    def __sub__(self, other):
        return Vector(self.x - other.x, self.y - other.y)
    
    def mag(self):
        """Method returns magnitude of the vector object
        """
        return math.sqrt((self.x**2) + (self.y**2))
    
    def dot(self, other):
        return (self.x * other.x) + (self.y * other.y)
    
    def angle(self, other):
        return math.acos(self.dot(other)/ (self.mag()* other.mag()))

if __name__ == "__main__":
    # test the Vector class
    v = Vector(2, 3)
    v1 = Vector(3, 5)
    print("v:", v)
    print("v.x:", v.x)
    print("v.y:", v.y)
    print("x.mag(): ", v.mag())
    print("v + v1 = ", v + v1)
    print("v - v1 = ", v - v1)
    # TASK 8
    v2= Vector(1,5) + Vector(-2, 3) - Vector(3, -4) + Vector(2, 1)
    print("Result of task 8",v2)
    print("Magnitude is", v2.mag())
    print("Vector(1,3).dot(Vector(4,2)) ==", Vector(1,3).dot(Vector(4,2)))
    v0 = Vector(1,0)
    w0 = Vector(1,1)
    ang = v0.angle(w0)
    print("The angle in radians = ",ang)
    print("The angle in degrees = ",math.degrees(ang))
    
    
    
    