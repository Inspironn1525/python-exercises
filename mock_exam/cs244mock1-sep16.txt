##################################################
# CS24420 Scientic Python Mock Exam 1 2019-02-25 #
##################################################
# Student Name:Sebastian Pasik
# Student Email:sep16@aber.ac.uk
########################################################################
# Include your answers (as comments), source code and code comments    #
# if applicable, but exclude any printing output or plotting result.   #
# Submit your file via Blackboard as PLAIN TEXT - no Word documents!   #
########################################################################

######################
# Answers to Part 1: #
######################
# a) I would use (3) Chi-square test of independence


##############################
# b)If this code is meant to return a list it should return a list instead of a dictionary
## I would change 'squares = {}' to be 'squares = []' 
## then check if function returns expected values 

######################
# Answers to Part 2: #
######################
# a) in order to calculate the odds of rolling 4 or less sixes, the scipy stars was imported
# then the probability of binomial distribution of cumultative densty function was calculated
# parameters: k = 4 means rolling at most 4 sixes
#	      n = 12 number of rolls per trial
#	      p = 1/6 probability of rolling six-sided dice

import scipy.stats as stats
pr = stats.binom.cdf(4, 12, 1/6)
print(pr)


##############################
# b)
import pandas as pd
# (1) Reading the data file
file = 'tree_heights.csv'
tree_data = pd.read_csv(file, sep=",")
# (2) Extracting the specified rows
ash_selector = (tree_data['Species'] == 'a')
ash_trees = tree_data[ash_selector]
print("Mean of the ash trees:", ash_trees['Height'].mean())
ash_mean = ash_trees['Height'].mean()
# (3) Extracting the oak trees
oak_selector = (tree_data['Species'] == 'o')
oak_trees = tree_data[oak_selector]
print("Mean of the oak trees:", oak_trees['Height'].mean())
oak_mean = oak_trees['Height'].mean()
# (4) For this scenario I would use Two sample t-test
p = stats.ttest_ind(a = ash_trees['Height'], b = oak_trees['Height'])
print(p)
#(5) 
# By default siginificance level is 0.05
# Since pvalue is approximately 0.059, we can accept the null hypothesis that 
# there is no significant difference between average height of oak or ashes trees


##############################
