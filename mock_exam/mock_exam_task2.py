# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 08:43:16 2019

@author: spasik
"""

import scipy.stats as stats
pr = stats.binom.cdf(4, 12, 1/6)
print(pr)

import pandas as pd
# (1) Reading the data file
file = 'tree_heights.csv'
tree_data = pd.read_csv(file, sep=",")
# (2) Extracting the specified rows
ash_selector = (tree_data['Species'] == 'a')
ash_trees = tree_data[ash_selector]
print("Mean of the ash trees:", ash_trees['Height'].mean())
ash_mean = ash_trees['Height'].mean()
# (3) Extracting the oak trees
oak_selector = (tree_data['Species'] == 'o')
oak_trees = tree_data[oak_selector]
print("Mean of the oak trees:", oak_trees['Height'].mean())
oak_mean = oak_trees['Height'].mean()
# (4) For this scenario I would use Two sample t-test
p = stats.ttest_ind(a = ash_trees['Height'], b = oak_trees['Height'])
print(p)
#(5) 
# By default siginificance level is 0.05
# Since pvalue is approximately 0.059, we can accept the null hypothesis that 
# there is no significant difference between average height of oak or ashes trees

