# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 17:20:05 2019

@author: sep16
"""
import pandas as pd

file = "HairEyeColor.csv"
he_color = pd.read_csv(file, sep=",")

#he_color.plot(kind='bar')
ecol = list(he_color.loc[:, 'Eye Colour'])
hcol = list(he_color.loc[:, 'Hair Colour'])
eyes = [0,0,0,0]
hair = [0,0,0,0]

for i in ecol:
    if i == 1:
        eyes[0]+= 1
    if i == 2:
        eyes[1]+= 1
    if i == 3:
        eyes[2]+= 1
    if i == 4:
        eyes[3]+= 1
        
for i in hcol:
    if i == 1:
        hair[0]+= 1
    if i == 2:
        hair[1]+= 1
    if i == 3:
        hair[2]+= 1
    if i == 4:
        hair[3]+= 1
print(eyes)
print(hair)